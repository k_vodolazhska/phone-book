﻿using System.Data.Entity;
using Autofac;
using Microsoft.Extensions.Configuration;
using PhoneBook.Data;
using PhoneBook.Data.Interfaces;
using PhoneBook.Data.Repositories;
using PhoneBook.Services;
using PhoneBook.Services.Interfaces;

namespace PhoneBook.Web.Modules
{
    public class AutofacModule : Autofac.Module
    {
        private readonly IConfigurationRoot _configuration;

        public AutofacModule(IConfigurationRoot configuration)
        {
            _configuration = configuration;
        }

        protected override void Load(ContainerBuilder builder)
        {
            builder.Register<DbContext>(c => new ContactDbContext(_configuration["Data:ContactConnectionString"]))
                .InstancePerLifetimeScope();
            builder.RegisterGeneric(typeof(Repository<>)).As(typeof(IRepository<>)).InstancePerLifetimeScope();
            builder.RegisterType(typeof(ContactService)).As(typeof(IContactService));
            builder.RegisterType(typeof(ContactsFileParser)).AsSelf();
        }
    }
}
