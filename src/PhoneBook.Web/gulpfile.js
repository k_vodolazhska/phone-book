﻿/// <binding />
/*
This file in the main entry point for defining Gulp tasks and using Gulp plugins.
Click here to learn more. http://go.microsoft.com/fwlink/?LinkId=518007
*/
"use strict";

var gulp = require('gulp');

gulp.task('default', function () {
    gulp.task('default', function () {
        gulp.src([
            'node_modules/core-js/client/**/*.{umd.js,js}',
            'node_modules/zone.js/dist/**/*.{umd.js,js}',
            'node_modules/reflect-metadata/**/*.{umd.js,js}',
            'node_modules/systemjs/dist/**/*.{umd.js,js, src.js}',
            'node_modules/@angular/core/bundles/**/*.{umd.js,js, src.js}',
            'node_modules/@angular/common/bundles/**/*.{umd.js,js, src.js}',
            'node_modules/@angular/compiler/bundles/**/*.{umd.js,js, src.js}',
            'node_modules/@angular/platform-browser/bundles/**/*.{umd.js,js, src.js}',
            'node_modules/@angular/platform-browser-dynamic/bundles/**/*.{umd.js,js, src.js}',
            'node_modules/@angular/http/bundles/**/*.{umd.js,js, src.js}',
            'node_modules/@angular/router/bundles/**/*.{umd.js,js, src.js}',
            'node_modules/@angular/forms/bundles/**/*.{umd.js,js, src.js}',
            'node_modules/angular-in-memory-web-api/bundles/**/*.{umd.js,js, src.js}',
            'node_modules/rxjs/**/*.{umd.js,js, src.js}'
        ], { 'base': 'node_modules' }).pipe(gulp.dest('./wwwroot/lib/node'));
    });
});


