﻿import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ContactsListComponent } from './contacts/contacts-list.component';
import { ContactComponent } from './contacts/contact.component';

const routes: Routes = [
    { path: '', redirectTo: '/contacts', pathMatch: 'full' },
    { path: 'contacts', component: ContactsListComponent },
    { path: 'contact/:id', component: ContactComponent },
    { path: 'contact', component: ContactComponent },
    { path: '**', redirectTo: 'contacts' }
];
@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule { }