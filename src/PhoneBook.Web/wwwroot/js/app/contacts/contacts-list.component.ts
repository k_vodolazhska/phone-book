﻿import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { IContactModel } from './IContactModel';

import { ContactService } from './contact.service';

@Component({
    moduleId: module.id,
    selector: 'contacts-list',
    templateUrl: 'contacts-list.component.html'
})
export class ContactsListComponent implements OnInit {
    private loadedContactsNumber: number = 0;

    contacts: IContactModel[] = [];

    constructor(
        private router: Router,
        private contactService: ContactService) { }

    loadMoreIsAvailable: boolean;

    ngOnInit(): void {
        this.loadMoreContacts();
    }

    loadMoreContacts(): void {
        this.contactService.getContacts(this.loadedContactsNumber)
            .then((data: IContactModel[]) => {
                if (data != undefined && data.length > 0) {
                    this.contacts = this.contacts.concat(data);
                    this.loadedContactsNumber += this.contacts.length;
                    this.loadMoreIsAvailable = true;
                } else {
                    this.loadMoreIsAvailable = false;
                }
            });
    }

    addContact(): void {
        this.router.navigate(['/contact']);
    }

    editContact(contactId: number): void {
        this.router.navigate(['/contact', contactId]);
    }

    uploadFile(event: any): void {
        if (event.target.files && event.target.files[0]) {

            var reader = new FileReader();

            reader.onload = (e: any) => {
                var data = e.target.result;

                this.contactService.uploadFile(data)
                .then((data: any) => {
                    if (data.success) {
                        this.loadedContactsNumber = 0;
                        this.loadMoreContacts();
                    }
                });
            }

            reader.readAsDataURL(event.target.files[0]);
        }
    }
}

