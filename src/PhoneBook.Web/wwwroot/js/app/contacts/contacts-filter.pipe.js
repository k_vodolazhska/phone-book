"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var ContactsFilterPipe = (function () {
    function ContactsFilterPipe() {
    }
    ContactsFilterPipe.prototype.transform = function (value, arg1, arg2) {
        var filter = arg1.toLocaleLowerCase();
        return filter ? value.filter(function (contact) { return contact[arg2].toLocaleLowerCase().indexOf(filter) !== -1; }) : value;
    };
    ContactsFilterPipe = __decorate([
        core_1.Pipe({
            name: 'contactsFilter'
        }), 
        __metadata('design:paramtypes', [])
    ], ContactsFilterPipe);
    return ContactsFilterPipe;
}());
exports.ContactsFilterPipe = ContactsFilterPipe;
//# sourceMappingURL=contacts-filter.pipe.js.map