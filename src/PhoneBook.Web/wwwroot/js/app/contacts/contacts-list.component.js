"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var router_1 = require('@angular/router');
var contact_service_1 = require('./contact.service');
var ContactsListComponent = (function () {
    function ContactsListComponent(router, contactService) {
        this.router = router;
        this.contactService = contactService;
        this.loadedContactsNumber = 0;
        this.contacts = [];
    }
    ContactsListComponent.prototype.ngOnInit = function () {
        this.loadMoreContacts();
    };
    ContactsListComponent.prototype.loadMoreContacts = function () {
        var _this = this;
        this.contactService.getContacts(this.loadedContactsNumber)
            .then(function (data) {
            if (data != undefined && data.length > 0) {
                _this.contacts = _this.contacts.concat(data);
                _this.loadedContactsNumber += _this.contacts.length;
                _this.loadMoreIsAvailable = true;
            }
            else {
                _this.loadMoreIsAvailable = false;
            }
        });
    };
    ContactsListComponent.prototype.addContact = function () {
        this.router.navigate(['/contact']);
    };
    ContactsListComponent.prototype.editContact = function (contactId) {
        this.router.navigate(['/contact', contactId]);
    };
    ContactsListComponent.prototype.uploadFile = function (event) {
        var _this = this;
        if (event.target.files && event.target.files[0]) {
            var reader = new FileReader();
            reader.onload = function (e) {
                var data = e.target.result;
                _this.contactService.uploadFile(data)
                    .then(function (data) {
                    if (data.success) {
                        _this.loadedContactsNumber = 0;
                        _this.loadMoreContacts();
                    }
                });
            };
            reader.readAsDataURL(event.target.files[0]);
        }
    };
    ContactsListComponent = __decorate([
        core_1.Component({
            moduleId: module.id,
            selector: 'contacts-list',
            templateUrl: 'contacts-list.component.html'
        }), 
        __metadata('design:paramtypes', [router_1.Router, contact_service_1.ContactService])
    ], ContactsListComponent);
    return ContactsListComponent;
}());
exports.ContactsListComponent = ContactsListComponent;
//# sourceMappingURL=contacts-list.component.js.map