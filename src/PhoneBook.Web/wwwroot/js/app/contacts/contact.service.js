"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var http_1 = require('@angular/http');
require('rxjs/add/operator/toPromise');
var ContactService = (function () {
    function ContactService(http) {
        this.http = http;
    }
    ContactService.prototype.getContacts = function (latestItem) {
        return this.http
            .get("api/contact/contacts/" + latestItem)
            .toPromise()
            .then(function (r) { return r.json(); })
            .catch(this.handleError);
    };
    ContactService.prototype.addContact = function (contactModel) {
        return this.http
            .post('api/contact', contactModel)
            .toPromise()
            .then(function (r) { return r.json(); })
            .catch(this.handleError);
    };
    ContactService.prototype.getContact = function (id) {
        return this.http
            .get("api/contact/" + id)
            .toPromise()
            .then(function (r) { return r.json(); })
            .catch(this.handleError);
    };
    ContactService.prototype.updateContact = function (contactModel) {
        return this.http
            .put("api/contact", contactModel)
            .toPromise()
            .then(function (r) { return r.json(); })
            .catch(this.handleError);
    };
    ContactService.prototype.uploadFile = function (data) {
        return this.http
            .post("api/contact/loadFromCsv", { data: data })
            .toPromise()
            .then(function (r) { return r.json(); })
            .catch(this.handleError);
    };
    ContactService.prototype.handleError = function (error) {
        console.error('An error occurred', error); // for debug only
        return Promise.reject(error.message || error);
    };
    ContactService = __decorate([
        core_1.Injectable(), 
        __metadata('design:paramtypes', [http_1.Http])
    ], ContactService);
    return ContactService;
}());
exports.ContactService = ContactService;
//# sourceMappingURL=contact.service.js.map