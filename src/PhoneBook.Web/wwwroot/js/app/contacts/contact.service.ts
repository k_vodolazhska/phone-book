﻿import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs';
import { IContactModel } from './IContactModel';

import 'rxjs/add/operator/toPromise';

@Injectable()
export class ContactService {
    constructor(private http: Http) { }

    getContacts(latestItem: number): Promise<IContactModel[]> {
        return this.http
            .get(`api/contact/contacts/${latestItem}`)
            .toPromise()
            .then((r: Response) => r.json() as IContactModel[])
            .catch(this.handleError);
    }

    addContact(contactModel: IContactModel): Promise<any> {
        return this.http
            .post('api/contact', contactModel)
            .toPromise()
            .then((r: Response) => r.json())
            .catch(this.handleError);
    }

    getContact(id: number): Promise<any> {
        return this.http
            .get(`api/contact/${id}`)
            .toPromise()
            .then((r: Response) => r.json() as IContactModel)
            .catch(this.handleError);
    }

    updateContact(contactModel: IContactModel): Promise<any> {
        return this.http
            .put("api/contact", contactModel)
            .toPromise()
            .then((r: Response) => r.json())
            .catch(this.handleError);
    }

    uploadFile(data: string) {
        return this.http
            .post("api/contact/loadFromCsv", { data })
            .toPromise()
            .then((r: Response) => r.json())
            .catch(this.handleError);
    }

    private handleError(error: any): Promise<any> {
        console.error('An error occurred', error); // for debug only
        return Promise.reject(error.message || error);
    }
}