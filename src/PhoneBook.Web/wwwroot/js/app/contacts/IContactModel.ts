﻿export interface IContactModel {
    id: number;
    firstName: string;
    lastName: string;
    phones: string[];
}

