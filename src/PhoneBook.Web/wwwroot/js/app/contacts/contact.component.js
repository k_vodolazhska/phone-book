"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var router_1 = require('@angular/router');
var contact_service_1 = require('./contact.service');
var ContactComponent = (function () {
    function ContactComponent(route, router, contactService) {
        this.route = route;
        this.router = router;
        this.contactService = contactService;
        this.contact = {
            id: 0,
            firstName: '',
            lastName: '',
            phones: ['']
        };
    }
    ContactComponent.prototype.removePhone = function (phoneIndex) {
        this.contact.phones.splice(phoneIndex, 1);
    };
    ContactComponent.prototype.addPhone = function () {
        this.contact.phones.push('');
    };
    ContactComponent.prototype.ngOnInit = function () {
        var _this = this;
        var contactId = this.route.params['id'];
        this.sub = this.route.params.subscribe(function (params) {
            if (params['id'] !== undefined)
                contactId = +params['id'];
            if (contactId !== undefined && contactId > 0) {
                _this.contactService.getContact(contactId)
                    .then(function (data) {
                    _this.contact = data;
                });
            }
        });
    };
    ContactComponent.prototype.ngOnDestroy = function () {
        this.sub.unsubscribe();
    };
    ContactComponent.prototype.onSubmit = function () {
        var _this = this;
        this.contact.phones = this.contact.phones.filter(function (p) { return p !== ''; });
        if (this.contact.id > 0) {
            this.contactService.updateContact(this.contact)
                .then(function (data) {
                if (data.success)
                    _this.router.navigate(['/contacts']);
            });
        }
        else {
            this.contactService.addContact(this.contact)
                .then(function (data) {
                if (data.success)
                    _this.router.navigate(['/contacts']);
            });
        }
    };
    ContactComponent = __decorate([
        core_1.Component({
            moduleId: module.id,
            selector: 'contact',
            templateUrl: 'contact.component.html'
        }), 
        __metadata('design:paramtypes', [router_1.ActivatedRoute, router_1.Router, contact_service_1.ContactService])
    ], ContactComponent);
    return ContactComponent;
}());
exports.ContactComponent = ContactComponent;
//# sourceMappingURL=contact.component.js.map