﻿import { Pipe, PipeTransform } from '@angular/core';
import { IContactModel } from './IContactModel';

@Pipe({
    name: 'contactsFilter'
})
export class ContactsFilterPipe implements PipeTransform {
    transform(value: any, arg1: string, arg2: string): any {
        let filter = arg1.toLocaleLowerCase();
        return filter ? value.filter((contact: IContactModel) => contact[arg2].toLocaleLowerCase().indexOf(filter) !== -1) : value;
    }
}