﻿import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

import { IContactModel } from './IContactModel';

import { ContactService } from './contact.service';

@Component({
    moduleId: module.id,
    selector: 'contact',
    templateUrl: 'contact.component.html'
})
export class ContactComponent implements OnInit, OnDestroy {
    private sub: any;

    constructor(
        private route: ActivatedRoute,
        private router: Router,
        private contactService: ContactService) { }

    contact: IContactModel = {
        id: 0,
        firstName: '',
        lastName: '',
        phones: ['']
    };

    removePhone(phoneIndex: number) {
        this.contact.phones.splice(phoneIndex, 1);
    }

    addPhone(): void {
        this.contact.phones.push('');
    }

    ngOnInit(): void {
        var contactId = this.route.params['id'];

        this.sub = this.route.params.subscribe(params => {
            if (params['id'] !== undefined)
                contactId = +params['id'];

            if (contactId !== undefined && contactId > 0) {
                this.contactService.getContact(contactId)
                    .then((data: IContactModel) => {
                        this.contact = data;
                    });
            }
        });
    }

    ngOnDestroy() {
        this.sub.unsubscribe();
    }

    onSubmit(): void {
        this.contact.phones = this.contact.phones.filter(p => p !== '');

        if (this.contact.id > 0) {
            this.contactService.updateContact(this.contact)
                .then((data) => {
                    if (data.success)
                        this.router.navigate(['/contacts']);
                });
        } else {
            this.contactService.addContact(this.contact)
                .then((data) => {
                    if (data.success)
                        this.router.navigate(['/contacts']);
                });
        }
    }
}