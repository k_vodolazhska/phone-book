﻿import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { RouterModule } from '@angular/router';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ContactsListComponent } from './contacts/contacts-list.component';
import { ContactComponent } from './contacts/contact.component';

import { ContactsFilterPipe } from './contacts/contacts-filter.pipe';

import { ContactService } from './contacts/contact.service';

@NgModule({
    imports: [
        BrowserModule,
        FormsModule,
        HttpModule,
        AppRoutingModule,
        RouterModule
    ],
    declarations: [
        AppComponent,
        ContactsListComponent,
        ContactComponent,
        ContactsFilterPipe],
    providers: [ContactService],
    bootstrap: [AppComponent]
})
export class AppModule { }