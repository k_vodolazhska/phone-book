/**
 * System configuration for Angular samples
 * Adjust as necessary for your application needs.
 */
(function (global) {
  System.config({
    paths: {
      // paths serve as alias
      'npm:': 'node_modules/'
    },
    // map tells the System loader where to look for things
    map: {
      // our app is within the app folder
      app: 'js/app',

      // angular bundles
      '@angular/core': 'lib/node/@angular/core/bundles/core.umd.js',
      '@angular/common': 'lib/node/@angular/common/bundles/common.umd.js',
      '@angular/compiler': 'lib/node/@angular/compiler/bundles/compiler.umd.js',
      '@angular/platform-browser': 'lib/node/@angular/platform-browser/bundles/platform-browser.umd.js',
      '@angular/platform-browser-dynamic': 'lib/node/@angular/platform-browser-dynamic/bundles/platform-browser-dynamic.umd.js',
      '@angular/http': 'lib/node/@angular/http/bundles/http.umd.js',
      '@angular/router': 'lib/node/@angular/router/bundles/router.umd.js',
      '@angular/forms': 'lib/node/@angular/forms/bundles/forms.umd.js',

      // other libraries
      'rxjs': 'lib/node/rxjs',
      'angular-in-memory-web-api': 'lib/node/angular-in-memory-web-api/bundles/in-memory-web-api.umd.js'
    },
    // packages tells the System loader how to load when no filename and/or no extension
    packages: {
      app: {
        main: './main.js',
        defaultExtension: 'js'
      },
      rxjs: {
        defaultExtension: 'js'
      }
    }
  });
})(this);
