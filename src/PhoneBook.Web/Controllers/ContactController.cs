﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;
using PhoneBook.Services;
using PhoneBook.Services.Interfaces;
using PhoneBook.Services.Models;
using PhoneBook.Web.Configuration;

namespace PhoneBook.Web.Controllers
{
    [Route("api/[controller]")]
    public class ContactController : Controller
    {
        private readonly IContactService _contactService;
        private readonly int _pageSize;
        private readonly ContactsFileParser _contactsFileParser;

        public ContactController(IServiceProvider serviceProvider, 
            IOptions<ConfigurationOptions> optionsAccessor, ContactsFileParser contactsFileParser)
        {
            _contactService = serviceProvider.GetService<IContactService>();
            _pageSize = optionsAccessor.Value.PageSize;
            _contactsFileParser = contactsFileParser;
        }

        [HttpGet("contacts/{latestLoadedIndex}")]
        public async Task<JsonResult> GetContacts([FromRoute]int latestLoadedIndex)
        {
            var contacts = await _contactService.GetFilteredContactsAsync(orderBy: c => c.Id, skip: latestLoadedIndex, take: _pageSize);

            return new JsonResult(contacts);
        }

        [HttpGet("{id}")]
        public async Task<JsonResult> Get([FromRoute] int id)
        {
            var contact = await _contactService.GetContactAsync(id);

            return new JsonResult(contact);
        }

        [HttpPost]
        public async Task<JsonResult> Post([FromBody]ContactModel contactModel)
        {
            var result = await _contactService.AddContactAsync(contactModel);

            return new JsonResult(new { Success = true });
        }

        [HttpPut]
        public async Task<JsonResult> Put([FromBody]ContactModel contactModel)
        {
            var result = await _contactService.UpdateContactAsync(contactModel);

            return new JsonResult(new { Success = true });
        }

        [HttpPost("loadFromCsv")]
        public async Task<JsonResult> LoadFromCsv([FromBody]FileData file)
        {
            if (!string.IsNullOrEmpty(file.Data))
            {
                var contactModels = _contactsFileParser.Parse(file.Data);

                await _contactService.AddContactsAsync(contactModels);

                return new JsonResult(new {Success = true});
            }

            return new JsonResult(new { Success = false });
        }
    }

    public class FileData
    {
        public string Data { get; set; }
    }
}
