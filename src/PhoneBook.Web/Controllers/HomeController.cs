﻿using Microsoft.AspNetCore.Mvc;

namespace PhoneBook.Web.Controllers
{
    public class HomeController : Controller
    {
        public IActionResult Index()
        {
            return View();
        }
    }
}
