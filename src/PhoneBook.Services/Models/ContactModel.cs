﻿namespace PhoneBook.Services.Models
{
    public class ContactModel
    {
        public int Id { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string[] Phones { get; set; }
        
    }
}
