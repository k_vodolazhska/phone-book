﻿using AutoMapper;
using PhoneBook.Data.Models;
using PhoneBook.Services.Models;

namespace PhoneBook.Services
{
    public class AutoMapperProfileConfiguration : Profile
    {
        public AutoMapperProfileConfiguration()
            : this("PhoneBookProfile")
        {
        }

        protected AutoMapperProfileConfiguration(string profileName)
            : base(profileName)
        {
            CreateMap<Contact, ContactModel>();

            CreateMap<ContactModel, Contact>()
                .ForMember(cM => cM.PhonesData, opt => opt.MapFrom(cM => string.Join(";", cM.Phones)));
        }
    }
}
