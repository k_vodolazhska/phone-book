﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Runtime.InteropServices;
using System.Threading.Tasks;
using AutoMapper;
using PhoneBook.Data.Interfaces;
using PhoneBook.Data.Models;
using PhoneBook.Services.Interfaces;
using PhoneBook.Services.Models;

namespace PhoneBook.Services
{
    public class ContactService : IContactService
    {
        private readonly IRepository<Contact> _contactRepository;
        private readonly IMapper _mapper;

        public ContactService(IRepository<Contact> contactRepository, IMapper mapper)
        {
            _contactRepository = contactRepository;
            _mapper = mapper;
        }

        public async Task<ContactModel> GetContactAsync(int contactId)
        {
            var contactEntity = await _contactRepository.GetByIdAsync(contactId);
            return _mapper.Map<Contact, ContactModel>(contactEntity);
        }

        public async Task<List<ContactModel>> GetFilteredContactsAsync(Expression<Func<Contact, bool>> predicate = null, Expression<Func<Contact, int>> orderBy = null, int take = 0, int skip = 0,
            params string[] navigationProperties)
        {
            var contacts = await _contactRepository.FilterAsync(predicate, orderBy, take, skip);
            return _mapper.Map<List<Contact>, List<ContactModel>>(contacts);
        }

        public async Task<int> AddContactAsync(ContactModel contactDto)
        {
            var contactEntity = _mapper.Map<ContactModel, Contact>(contactDto);

            _contactRepository.Add(contactEntity);
            return await _contactRepository.SaveChangesAsync();
        }

        public async Task<int> AddContactsAsync(List<ContactModel> contactsDto)
        {
            var contactEntities = _mapper.Map<List<ContactModel>, List<Contact>>(contactsDto);

            _contactRepository.Add(contactEntities);

            return await _contactRepository.SaveChangesAsync();
        }

        public async Task<int> UpdateContactAsync(ContactModel contactDto)
        {
            var contactEntity = _mapper.Map<ContactModel, Contact>(contactDto);

            _contactRepository.Update(contactEntity);

            return await _contactRepository.SaveChangesAsync();
        }
    }
}
