﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;
using PhoneBook.Data.Models;
using PhoneBook.Services.Models;

namespace PhoneBook.Services.Interfaces
{
    public interface IContactService
    {
        Task<ContactModel> GetContactAsync(int contactId);

        Task<List<ContactModel>> GetFilteredContactsAsync(Expression<Func<Contact, bool>> predicate = null,
            Expression<Func<Contact, int>> orderBy = null,
            int take = 0, int skip = 0, params string[] navigationProperties);

        Task<int> AddContactAsync(ContactModel contactDto);

        Task<int> AddContactsAsync(List<ContactModel> contactsDto);

        Task<int> UpdateContactAsync(ContactModel contactDto);

    }
}
