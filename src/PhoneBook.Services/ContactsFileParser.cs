﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PhoneBook.Services.Models;

namespace PhoneBook.Services
{
    public class ContactsFileParser
    {
        public List<ContactModel> Parse(string fileData)
        {
            var contactModels = new List<ContactModel>();

            byte[] data = Convert.FromBase64String(fileData.Substring(fileData.IndexOf(',') + 1));
            string decodedString = Encoding.UTF8.GetString(data);

            var contactLines = decodedString.Split(new [] {Environment.NewLine},
                StringSplitOptions.RemoveEmptyEntries).ToList();

            foreach (var contactLine in contactLines)
            {
                var contactProperties = contactLine.Split(',');

                var contectModel = new ContactModel
                {
                    FirstName = contactProperties[0],
                    LastName = contactProperties[1],
                    Phones = contactProperties[2].Split(';')
                };

                contactModels.Add(contectModel);
            }

            return contactModels;
        }
    }
}
