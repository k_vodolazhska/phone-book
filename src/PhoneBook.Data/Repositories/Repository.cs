﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using PhoneBook.Data.Interfaces;
using PhoneBook.Data.Models;

namespace PhoneBook.Data.Repositories
{
    public class Repository<T> : IRepository<T>, IDisposable where T : class, IEntity
    {
        private IQueryable<T> FilterInternal(Expression<Func<T, bool>> predicate = null,
            Expression<Func<T, int>> orderBy = null,
            int take = 0, int skip = 0)
        {
            var query = DbSet.AsQueryable();

            if (predicate != null)
                query = query.Where(predicate);

            if (orderBy != null)
                query = query.OrderBy(orderBy);

            if (skip > 0)
                query = query.Skip(skip);

            if (take > 0)
                query = query.Take(take);

            return query;
        }

        private bool _disposed;

        public Repository(DbContext dbContext)
        {
            if (dbContext == null)
                throw new ArgumentNullException(nameof(dbContext));
            DbContext = dbContext;
            DbSet = DbContext.Set<T>();
        }

        protected virtual DbContext DbContext { get; set; }
        protected virtual DbSet<T> DbSet { get; set; }

        protected virtual void Dispose(bool disposing)
        {
            if (!_disposed)
            {
                if (disposing)
                {
                    DbContext.Dispose();
                }
            }
            _disposed = true;
        }
        
        public IQueryable<T> GetAll()
        {
            return DbSet;
        }

        public Task<List<T>> FilterAsync(Expression<Func<T, bool>> predicate = null,
            Expression<Func<T, int>> orderBy = null,
            int take = 0, int skip = 0)
        {
            var query = FilterInternal(predicate, orderBy, take, skip);

            return query.ToListAsync();
        }
        
        public virtual Task<T> GetByIdAsync(int id)
        {
            return DbSet.FindAsync(id);
        }

        public virtual void Add(T entity)
        {
            var dbEntityEntry = DbContext.Entry(entity);
            if (dbEntityEntry.State != EntityState.Detached)
            {
                dbEntityEntry.State = EntityState.Added;
            }
            else
            {
                DbSet.Add(entity);
            }
        }

        public virtual void Update(T entity)
        {
            var dbEntityEntry = DbContext.Entry(entity);
            if (dbEntityEntry.State == EntityState.Detached)
            {
                DbSet.Attach(entity);
            }
            dbEntityEntry.State = EntityState.Modified;
        }

        public void Add(IEnumerable<T> entities)
        {
            DbSet.AddRange(entities);
        }

        public Task<int> SaveChangesAsync()
        {
            return DbContext.SaveChangesAsync();
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        
    }
}
