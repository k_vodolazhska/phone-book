﻿using System.Data.Entity;
using PhoneBook.Data.Models;

namespace PhoneBook.Data
{
    public class ContactDbContext: DbContext
    {
        static ContactDbContext()
        {
            Database.SetInitializer(new DropCreateDatabaseIfModelChanges<ContactDbContext>());
        }

        public ContactDbContext(string connectionString) : base(connectionString)
        {
            Configuration.LazyLoadingEnabled = false;
        }

        public virtual DbSet<Contact> Contact { get; set; }
    }
}
