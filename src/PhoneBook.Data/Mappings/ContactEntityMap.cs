﻿using System.Data.Entity.ModelConfiguration;
using PhoneBook.Data.Models;

namespace PhoneBook.Data.Mappings
{
    public class ContactEntityMap : EntityTypeConfiguration<Contact>
    {
        public ContactEntityMap()
        {
            // Table Mappings
            ToTable("Contact");

            // Primary Key
            HasKey(s => s.Id);

            // Column Mappings
            Property(p => p.Id)
                .HasColumnName("ContactId");

            Property(p => p.FirstName)
                .HasMaxLength(100)
                .IsRequired();

            Property(p => p.LastName)
                .HasMaxLength(100)
                .IsRequired();

            Property(p => p.PhonesData)
                .HasMaxLength(100)
                .IsRequired();

            Property(p => p.PhonesData)
                .HasMaxLength(150)
                .IsRequired();

            Ignore(p => p.Phones);
        }
    }
}

