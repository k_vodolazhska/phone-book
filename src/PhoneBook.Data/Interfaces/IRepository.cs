﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using PhoneBook.Data.Models;

namespace PhoneBook.Data.Interfaces
{
    public interface IRepository<T> where T : class, IEntity
    {
        #region Non-Async Methods
        
        IQueryable<T> GetAll();
        void Add(T entity);
        void Add(IEnumerable<T> entities);
        void Update(T entity);

        #endregion

        #region Async Methods

        Task<List<T>> FilterAsync(Expression<Func<T, bool>> predicate = null,
            Expression<Func<T, int>> orderBy = null,
            int take = 0, int skip = 0);
        Task<T> GetByIdAsync(int id);
        Task<int> SaveChangesAsync();

        #endregion
    }
}
