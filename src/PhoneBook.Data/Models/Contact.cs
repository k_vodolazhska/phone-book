﻿namespace PhoneBook.Data.Models
{
    public class Contact : IEntity
    {
        public int Id { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string PhonesData { get; set; }

        public string[] Phones => PhonesData.Split(';');
    }
}
